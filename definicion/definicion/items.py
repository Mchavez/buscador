# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class DefinicionItem(scrapy.Item):
    # nombre = scrapy.Field()
    descripcion = scrapy.Field()
    # pass
